﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ArticlesServer.Models
{
    public class Article
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime DateModified { get; set; }

        // Foreign key
        public int ArticleTextId { get; set; }
    }
}