﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ArticlesServer.Models
{
    public class ArticleText
    {
        public int Id { get; set; }

        [Column(TypeName = "nvarchar(MAX)")]
        public string Text { get; set; }
    }
}