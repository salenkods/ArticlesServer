﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ArticlesServer.Models
{
    public class ArticleDetailsDTO
    {
        public string Title { get; set; }
        public string Text { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateModified { get; set; }
    }
}