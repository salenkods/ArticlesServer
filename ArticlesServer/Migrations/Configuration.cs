namespace ArticlesServer.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<ArticlesServer.Models.ArticlesServerContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "ArticlesServer.Models.ArticlesServerContext";
        }

        protected override void Seed(ArticlesServer.Models.ArticlesServerContext context)
        {
            // init 
            var articleText1 = new Models.ArticleText()
            {
                Id = 1,
                Text = "Once upon a time there lived a lion in a forest. One day after a heavy meal. It was sleeping under a tree. After a while, there came a mouse and it started to play on the lion. Suddenly the lion got up with anger and looked for those who disturbed its nice sleep. Then it saw a small mouse standing trembling with fear. The lion jumped on it and started to kill it. The mouse requested the lion to forgive it. The lion felt pity and left it. The mouse ran away. On another day, the lion was caught in a net by a hunter.The mouse came there and cut the net.Thus it escaped.There after, the mouse and the lion became friends.They lived happily in the forest afterwards."
            };
            var articleText2 = new Models.ArticleText()
            {
                Id = 2,
                Text = "A Town Mouse and a Country Mouse were friends. The Country Mouse one day invited his friend to come and see him at his home in the fields. The Town Mouse came and they sat down to a dinner of barleycorns and roots the latter of which had a distinctly earthy flavour. The flavour was not much to the taste of the guest and presently he broke out with \"My poor dear friend, you live here no better than the ants. Now, you should just see how I fare!My larder is a regular horn of plenty.You must come and stay with me and I promise you shall live on the fat of the land.\" So when he returned to town he took the Country Mouse with him and showed him into a larder containing flour and oatmeal and figs and honey and dates. The Country Mouse had never seen anything like it and sat down to enjoy the luxuries his friend provided.But before they had well begun, the door of the larder opened and some one came in. The two Mice scampered off and hid themselves in a narrow and exceedingly uncomfortable hole.Presently, when all was quiet, they ventured out again.But some one else came in, and off they scuttled again.This was too much for the visitor. \"Good bye,\" said he, \"I'm off. You live in the lap of luxury, I can see, but you are surrounded by dangers whereas at home I can enjoy my simple dinner of roots and corn in peace.\""
            };
            context.ArticleTexts.AddOrUpdate(a => a.Id, articleText1, articleText2);
            context.SaveChanges();

            var article1 = new Models.Article()
            {
                Id = 1,
                Title = "A friend in need is a friend indeed",
                DateCreated = DateTime.UtcNow,
                DateModified = DateTime.UtcNow,
                ArticleTextId = articleText1.Id
            };
            var article2 = new Models.Article()
            {
                Id = 2,
                Title = "A Town Mouse and A Country Mouse",
                DateCreated = DateTime.UtcNow,
                DateModified = DateTime.UtcNow,
                ArticleTextId = articleText2.Id
            };
            context.Articles.AddOrUpdate(a => a.Id, article1, article2);
        }
    }
}
