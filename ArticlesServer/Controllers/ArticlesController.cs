﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using ArticlesServer.Models;

namespace ArticlesServer.Controllers
{
    public class ArticlesController : ApiController
    {
        private ArticlesServerContext db = new ArticlesServerContext();

        // GET: api/Articles
        public IQueryable<ArticleDTO> GetArticles()
        {
            var articles = db.Articles
                .Select(a => new ArticleDTO()
                {
                    Id = a.Id,
                    Title = a.Title,
                    DateCreated = a.DateCreated,
                    DateModified = a.DateModified
                });
            return articles;
        }

        // GET: api/Articles/5
        [ResponseType(typeof(ArticleDetailsDTO))]
        public IHttpActionResult GetArticle(int id)
        {
            Article article = db.Articles.Find(id);
            if (article == null)
            {
                return NotFound();
            }

            var dto = new ArticleDetailsDTO()
            {
                Title = article.Title,
                DateCreated = article.DateCreated,
                DateModified = article.DateModified
            };

            ArticleText articleText = db.ArticleTexts.Find(article.ArticleTextId);
            if (articleText == null)
            {
                return NotFound();
            }

            dto.Text = articleText.Text;

            return Ok(dto);
        }

        // PUT: api/Articles/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutArticle(int id, ArticleDetailsDTO articleDetails)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            // edit article
            Article article = db.Articles.Find(id);
            if (article == null)
            {
                return NotFound();
            }

            article.Title = articleDetails.Title;
            article.DateModified = articleDetails.DateModified;
            db.Entry(article).State = EntityState.Modified;

            // edit article's text
            ArticleText articleText = db.ArticleTexts.Find(article.ArticleTextId);
            if (articleText == null)
            {
                return NotFound();
            }

            articleText.Text = articleDetails.Text;
            db.Entry(articleText).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ArticleExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Articles
        [ResponseType(typeof(ArticleDTO))]
        public IHttpActionResult PostArticle(ArticleDetailsDTO articleDetails)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            ArticleText articleText = new ArticleText()
            {
                Text = articleDetails.Text
            };
            db.ArticleTexts.Add(articleText);
            db.SaveChanges();

            Article article = new Article()
            {
                ArticleTextId = articleText.Id,
                Title = articleDetails.Title,
                DateCreated = articleDetails.DateCreated,
                DateModified = articleDetails.DateModified
            };
            db.Articles.Add(article);
            db.SaveChanges();

            // returned object
            var dto = new ArticleDTO()
            {
                Id = article.Id,
                Title = article.Title,
                DateCreated = article.DateCreated,
                DateModified = article.DateModified
            };

            return CreatedAtRoute("DefaultApi", new { id = article.Id }, dto);
        }

        // DELETE: api/Articles/5
        [ResponseType(typeof(Article))]
        public IHttpActionResult DeleteArticle(int id)
        {
            Article article = db.Articles.Find(id);
            if (article == null)
            {
                return NotFound();
            }

            var articleText = db.ArticleTexts.Find(article.ArticleTextId);
            if (articleText == null)
            {
                return NotFound();
            }

            db.Articles.Remove(article);
            db.ArticleTexts.Remove(articleText);
            db.SaveChanges();

            return Ok(article);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ArticleExists(int id)
        {
            return db.Articles.Count(e => e.Id == id) > 0;
        }
    }
}